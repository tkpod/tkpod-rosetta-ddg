# tkpod-rosetta-ddg

[![conda](https://img.shields.io/conda/dn/tkpod/tkpod-rosetta-ddg.svg)](https://anaconda.org/tkpod/tkpod-rosetta-ddg/)
[![docs](https://img.shields.io/badge/docs-v0.1.3-blue.svg)](https://tkpod.gitlab.io/tkpod-rosetta-ddg/v0.1.3/)
[![build status](https://gitlab.com/tkpod/tkpod-rosetta-ddg/badges/v0.1.3/build.svg)](https://gitlab.com/tkpod/tkpod-rosetta-ddg/commits/v0.1.3/)
[![coverage report](https://gitlab.com/tkpod/tkpod-rosetta-ddg/badges/v0.1.3/coverage.svg)](https://tkpod.gitlab.io/tkpod-rosetta-ddg/v0.1.3/htmlcov/)

Rosetta ΔΔG plugin for `tkpod`.
